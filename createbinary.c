/*For generating bin file for 1 mb, I have generated the following program.*/

#include <stdio.h>
#include <stdlib.h>

const unsigned int wanted_size = 1048576;

int main()
{

        FILE *fptr;

        if ((fptr = fopen("fil1.bin","wb")) == NULL){
                printf("Error! opening file");
                exit(1);
        }

        fseek(fptr, wanted_size - 1, SEEK_SET);

        fputc('\0',fptr);

//      fseek(fptr, 0, SEEK_SET);

//      fwrite("010101111010111010", 1, 16, fptr);

        fseek(fptr, wanted_size - 1000, SEEK_SET);

        /*For creating first file, fil1.bin, uncomment the following statement.*/
        fwrite("010101111010111010", 1, 16, fptr); //fil1.bin, file1

        /*For creating second file, fil2.bin, uncomment the following statement. */
//      fwrite("010110111010110011", 1, 16, fptr); //fil2.bin, file2

        fclose(fptr);

        return 0;
}