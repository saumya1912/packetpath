#include <stdio.h>
#include <time.h>

#define BYTESSIZE 16

void compFiles (FILE *f1, FILE *f2){

	char c1, c2, buffer1[20]={0}, buffer2[20]={0};

	/*The loop begins for reading two files till end of file.*/
	while((c1 = fgetc(f1)) != EOF && (c2 = fgetc(f2)) != EOF){
		
		if(c1 == c2){
//			printf("The char is same.\n");
			continue;
		}

		else{

			
			printf("Current position in file1: %ld\n", ftell(f1));

            /*Setting the pointer to previous byte so as to display 16 bytes from that byte.*/
			fseek(f1, -1, SEEK_CUR);
            
            /*Reading 16 bytes from the byte which differs.*/
			fread(buffer1, BYTESSIZE, 1, f1);
			printf("16 bytes from current position in file1: %s\n\n", buffer1);
			

			printf("Current position in file2: %ld\n", ftell(f2));

            /*Setting the pointer to previous byte so as to display 16 bytes from that byte.*/
			fseek(f2, -1, SEEK_CUR);
            
            /*Reading 16 bytes from the byte which differs.*/
			fread(buffer2, BYTESSIZE, 1, f2);
			
			printf("16 bytes from current position in file2: %s\n\n", buffer2);
			
			break;
		}
	}	

}


int main(int argc, char *argv[]){

	FILE *f1, *f2;
	double comptime;

	/*The number of arguments should be 2 as there are 2 files. */
	if (argc != 3){
		printf("Two file names expected and the Syntax is ./exe file1 file2. \n");
		return -1;
	}

	else{
		printf("Name of first file: %s\n", argv[1]);
		printf("Name of second file: %s\n\n", argv[2]);

		/*Opening first binary file.*/
		f1 = fopen(argv[1], "rb");

		if(f1 == NULL){
			printf("Error opening file1.\n");
			return -1;
		}

		/*Opening second binary file.*/
		f2 = fopen(argv[2], "rb");
		
		if(f2 == NULL){
			printf("Error opening file 2.\n");
			return -1;
		}

		/*This calculates start time of execution of function compFiles.*/
		clock_t start = clock();

		compFiles (f1, f2);

		/*This calculates end time of execution of function compFiles.*/
		clock_t end = clock();

		/*This gives total computation time for the program.*/
		comptime = ((double)(end-start)) / CLOCKS_PER_SEC;

		printf("Time taken for computation: %f\n", comptime);

		fclose(f1);
		fclose(f2);
		return 0; 

	}

}
